from .development import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = []

password_validation_str = 'django.contrib.auth.password_validation'
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': f'{password_validation_str}.UserAttributeSimilarityValidator',
    },
    {
        'NAME': f'{password_validation_str}.MinimumLengthValidator',
    },
    {
        'NAME': f'{password_validation_str}.CommonPasswordValidator',
    },
    {
        'NAME': f'{password_validation_str}.NumericPasswordValidator',
    },
]

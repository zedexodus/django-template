# Django Project/App Templates

## Django Project Template

To use this repository just use the `template` option of `django-admin`
[`start project`](https://docs.djangoproject.com/en/4.0/ref/django-admin/#startproject)

```
$ django-admin startproject --template=https://gitlab.com/zedexodus/django-template/-/archive/master/django-template-master.zip [projectname]
```
